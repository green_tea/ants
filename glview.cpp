#include "glview.hpp"

glview::glview()
{

}

void glview::initializeGL() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, 800, 600, 0, 0, 1);
}

void glview::resizeGL(int w, int h) {
    glViewport(0, 0, w, h);

}

void glview::paintGL() {
    qglClearColor(Qt::white);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    qglColor(Qt::green);
    glBegin(GL_LINE_LOOP);

    glEnd();
}

void glview::drawAll(const Map &map) {
    QScreen screen = QGuiApplication::primaryScreen();
    QRect screen_geometry = screen->geometry();
    int height = screen_geometry.height();
    int width = screen_geometry.width();

    Size map_size = map.getSize();

    int block_height = height / map_size.get_h();
    int block_width = width / map_size.get_w();

    for (int i = 0; i < map_size.get_w(); ++i) {
        for (int j = 0; j < map_size.get_h(); ++j) {
            MapBlock block = map.get_block(i, j);

            drawBlock(block, i * block_width, j * block_height, block_width, block_height);
        }
    }
}

void glview::drawBlock(const MapBlock &block, int x, int y, int w, int h) {
    int green = block.get_green_color();

    glColor3f(0, green, 0);
    glBegin(GL_FILL_RECTANGLE_NV);
        glVertex2f(x, y);
        glVertex2f(x + w, y);
        glVertex2f(x + w, y + h);
        glVertex2f(x, y + h);
    glEnd();
}
