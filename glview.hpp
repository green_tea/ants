#ifndef GLVIEW_HPP
#define GLVIEW_HPP

#include <QGLWidget>

#include "Map/Map.h"

class glview : public QGLWidget
{
private:

    void drawBlock(const MapBlock& block, int x, int y, int w, int h);

public:
    glview();

    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

    void drawAll(const Map& map);

};

#endif // GLVIEW_HPP
