//
// Created by ice_tea on 15.04.19.
//

#ifndef ANTS_TEAM_H
#define ANTS_TEAM_H

#include <vector>

#include "../Ant/Ant.h"
#include "../Map/Map.h"

using std::vector;

template <typename Brain>
class Team {
private:
    vector<Ant<Brain>> ants;

public:
    void next_tick(const Map& map);


};


#endif //ANTS_TEAM_H
