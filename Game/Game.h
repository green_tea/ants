//
// Created by ice_tea on 15.04.19.
//

#ifndef ANTS_GAME_H
#define ANTS_GAME_H

#include <vector>

#include "Team.h"
#include "../Map/Map.h"
#include "glview.h"

using std::vector;

template <typename FirstBrain, typename SecondBrain, typename MapGenerator>
class Game {
private:
    Team<FirstBrain> first_team;
    Team<SecondBrain> second_team;

    Map<MapGenerator> map;
    glview form;

public:
    void start_game();

    Game(Team<FirstBrain> first_team, Team<SecondBrain> second_team, Size map_size);
};


#endif //ANTS_GAME_H
