//
// Created by ice_tea on 15.04.19.
//


#include "Game.h"

template<typename FirstBrain, typename SecondBrain, typename MapGenerator>
void Game::start_game()
{
    form.drawAll(map);
}

template<typename FirstBrain, typename SecondBrain, typename MapGenerator>
Game::Game(Team<FirstBrain> first_team, Team<SecondBrain> second_team, Size map_size)
{
    this->first_team = first_team;
    this->second_team = second_team;

    this->map = Map<MapGenerator>(map_size);

}
