//
// Created by ice_tea on 15.04.19.
//

#ifndef ANTS_SIZE_H
#define ANTS_SIZE_H


#include <cstddef>

class Size {
private:
    size_t x, y;

public:
    size_t get_x() const ;

    size_t get_y() const ;

    static Size get_default_size();

    Size(size_t x, size_t y);
};


#endif //ANTS_SIZE_H
