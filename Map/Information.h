//
// Created by ice_tea on 19.04.19.
//

#ifndef ANTS_INFORMATION_H
#define ANTS_INFORMATION_H

#include <vector>

#include "MapBlock.h"

class Information {
private:
    std::vector<MapBlock> data;

public:

    const MapBlock& get_left_up_data();

    const MapBlock& get_up_data();

    const MapBlock& get_right_up_data();

    const MapBlock& get_right_data();

    const MapBlock& get_right_down_data();

    const MapBlock& get_down_data();

    const MapBlock& get_left_down_data();

    const MapBlock& get_left_data();

    const MapBlock& get_center_data();

    explicit Information(std::vector<MapBlock>& $data);

};


#endif //ANTS_INFORMATION_H
