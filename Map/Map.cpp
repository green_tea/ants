//
// Created by ice_tea on 15.04.19.
//

#include "Map.h"

bool Map::check_sizes(int x, int y) {
    if (x > size.get_x() || x < 0)
        return false;
    if (y > size.get_y() || y < 0)
        return false;
}

Information Map::get_information(int x, int y) {
    if (!check_sizes(x, y))
        return nullptr;

    vector<MapBlock> result;
    result.push_back(get_block(x, y));

    if (check_sizes(x - 1, y))
        result.push_back(get_block(x - 1, y));

    if (check_sizes(x - 1, y + 1))
        result.push_back(get_block(x - 1, y + 1));

    if (check_sizes(x, y + 1))
        result.push_back(get_block(x, y + 1));

    if (check_sizes(x + 1, y + 1))
        result.push_back(get_block(x + 1, y + 1));

    if (check_sizes(x + 1, y))
        result.push_back(get_block(x + 1, y));

    if (check_sizes(x + 1, y - 1))
        result.push_back(get_block(x + 1, y - 1));

    if (check_sizes(x, y - 1))
        result.push_back(get_block(x, y - 1));

    if (check_sizes(x - 1, y - 1))
        result.push_back(get_block(x - 1, y - 1));

    return Information(result);
}

MapBlock &Map::get_block(int x, int y) {
    return map[x][y];
}

Map::Map(Size size) : size(size) {
    map.resize(size.get_x());

    for (size_t i = 0; i < size.get_x(); ++i) {
        map[i].resize(size.get_y());

        for (size_t j = 0; j < size.get_y(); ++j) {

            std::pair<int, int> bounds = generator.generate();
            map[i][j] = MapBlock(bounds.first, bounds.second);
        }
    }


}
