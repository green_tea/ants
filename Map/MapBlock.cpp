//
// Created by ice_tea on 15.04.19.
//

#include "MapBlock.h"

int MapBlock::get_food() {
    return food;
}

vector<Ant> &MapBlock::get_ants() {
    return ants;
}

MapBlock::MapBlock(int l, int r) {
    this->food = Random::get_random(l, r);
}

MapBlock::MapBlock() {
    this->food = 0;
}
