//
// Created by ice_tea on 19.04.19.
//

#include "Information.h"

const MapBlock &Information::get_left_up_data() {
    return data[0];
}

const MapBlock &Information::get_up_data() {
    return data[1];
}

const MapBlock &Information::get_right_up_data() {
    return data[2];
}

const MapBlock &Information::get_right_data() {
    return data[3];
}

const MapBlock &Information::get_right_down_data() {
    return data[4];
}

const MapBlock &Information::get_down_data() {
    return data[5];
}

const MapBlock &Information::get_left_down_data() {
    return data[6];
}

const MapBlock &Information::get_left_data() {
    return data[7];
}

const MapBlock &Information::get_center_data() {
    return data[8];
}

Information::Information(std::vector<MapBlock> &$data) {

    this->data.resize(data.size());

    for (int i = 0; i < data.size(); ++i) {
        this->data[i] = data[i];
    }
}
