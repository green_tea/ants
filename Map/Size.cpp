//
// Created by ice_tea on 15.04.19.
//

#include "Size.h"

size_t Size::get_x() const {
    return x;
}

size_t Size::get_y() const {
    return y;
}

Size Size::get_default_size() {
    return Size(100, 100);
}

Size::Size(size_t x, size_t y) {
    this->x = x;
    this->y = y;
}
