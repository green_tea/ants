//
// Created by ice_tea on 15.04.19.
//

#include "SimpleGenerator.h"

std::pair<int, int> SimpleGenerator::generate(int x, int y) const {
    int second_x = abs(x - map_size.get_x());
    int second_y = abs(y - map_size.get_y());

    return std::pair(std::min(x, second_x), std::min(y, second_y));
}
