//
// Created by ice_tea on 15.04.19.
//

#ifndef ANTS_SIMPLEGENERATOR_H
#define ANTS_SIMPLEGENERATOR_H

#include <cmath>
#include <algorithm>

#include "MapGenerator.h"

using std::abs;

class SimpleGenerator : public MapGenerator {
public:
    std::pair<int, int> generate(int x, int y) const override;

    explicit SimpleGenerator(const Size& size) : MapGenerator(size) {};
};


#endif //ANTS_SIMPLEGENERATOR_H
