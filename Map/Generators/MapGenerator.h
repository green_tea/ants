//
// Created by ice_tea on 15.04.19.
//

#ifndef ANTS_MAPGENERATOR_H
#define ANTS_MAPGENERATOR_H

#include <utility>

#include "../Size.h"

class MapGenerator {
protected:
    Size map_size;

public:

    virtual std::pair<int, int> generate(int x, int y) const = 0;

    explicit MapGenerator(const Size& map_size) : map_size(map_size) {}

};


#endif //ANTS_MAPGENERATOR_H
