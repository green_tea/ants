//
// Created by ice_tea on 15.04.19.
//

#ifndef ANTS_RANDOM_H
#define ANTS_RANDOM_H

#include <random>

class Random {
private:
    static std::default_random_engine generator;

public:

    static int get_random(int l, int r);

};


#endif //ANTS_RANDOM_H
