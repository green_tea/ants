//
// Created by ice_tea on 15.04.19.
//

#ifndef ANTS_MAPBLOCK_H
#define ANTS_MAPBLOCK_H

#include <vector>

#include "../Ant/Ant.h"
#include "../Ant/Brains/Brain.h"
#include "Random.h"

using std::vector;

class MapBlock {
    int food;
    vector<Ant<Brain>> ants;

public:

    int get_food();

    vector<Ant<Brain>>& get_ants();

    MapBlock(int l, int r);

    MapBlock();

    int get_green_color();

};


#endif //ANTS_MAPBLOCK_H
