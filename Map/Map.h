//
// Created by ice_tea on 15.04.19.
//

#ifndef ANTS_MAP_H
#define ANTS_MAP_H


#include <vector>

#include "MapBlock.h"
#include "Size.h"
#include "Information.h"

using std::vector;

template <typename Generator>
class Map {
private:
    vector<vector<MapBlock> > map;
    Size size;
    Generator generator;

    bool check_sizes(int x, int y);


public:
    Information get_information(int i, int j);

    Map(Size size);

    Size getSize();

    MapBlock& get_block(int x, int y);

};


#endif //ANTS_MAP_H
