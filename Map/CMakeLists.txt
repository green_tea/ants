add_executable(map
        Map.h Map.cpp
        MapBlock.h
        MapBlock.cpp
        Size.h
        Size.cpp
        Random.cpp
        Random.h
        Information.cpp
        Information.h

        ../Ant/Ant.h)

add_subdirectory(Generators)