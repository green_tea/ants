#-------------------------------------------------
#
# Project created by QtCreator 2019-04-25T21:53:30
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ants_with_qt
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    glview.cpp \
    Game/Game.cpp \
    Game/Team.cpp \
    Map/Generators/SimpleGenerator.cpp \
    Map/Map.cpp \
    Map/Information.cpp \
    Map/Random.cpp \
    Map/Size.cpp \
    Map/MapBlock.cpp \
    Ant/Ant.cpp \
    Ant/Brains/SimpleBrain.cpp \
    Ant/Brains/Brain.cpp

HEADERS += \
        mainwindow.h \
    glview.hpp \
    Game/Game.h \
    Game/Team.h \
    Map/Generators/SimpleGenerator.h \
    Map/Generators/MapGenerator.h \
    Map/Information.h \
    Map/Map.h \
    Map/Random.h \
    Map/Size.h \
    Map/MapBlock.h \
    Ant/Ant.h \
    Ant/Brains/SimpleBrain.h \
    Ant/Brains/Brain.h

FORMS += \
        mainwindow.ui

DISTFILES +=
