#include "mainwindow.h"
#include <QApplication>
#include "Game/Game.h"
#include "Ant/Brains/SimpleBrain.h"
#include "Map/Generators/SimpleGenerator.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Team<SimpleBrain> first_team;
    Team<SimpleBrain> second_team;

    Size game_size(40, 40);

    Game<SimpleBrain, SimpleBrain, SimpleGenerator> game(first_team, second_team, game_size);

    game.start_game();


    return a.exec();
}
